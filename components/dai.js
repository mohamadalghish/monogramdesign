import styles from "@styles/Ai.module.css";
import DTag from "@components/dtag";

export default function Dai({ page }) {
  return (
    <>
      <div className="grid grid-cols-1">
        <div className={styles.container_ai_div}>
          <div className="grid grid-cols-12">
            <div>
            </div>
            <div className="lg:col-span-7 md:col-span-12 sm:col-span-12">
              <DTag APostion='relative' Text='Person' Atop='96' Aleft='0'></DTag>
              <DTag APostion='relative' Text='Desk' Atop='72' Aleft='36'></DTag>
              <DTag APostion='relative' Text='Person' Atop='20' Aleft='72'></DTag>
              <DTag APostion='relative' Text='Pencils' Atop='40' Aleft='96'></DTag>
            </div>
            <div className="grid grid-rows-3 lg:col-span-3 md:col-span-12 sm:col-span-12 md:m-2 sm:m-2">

              <div className="">
                <DTag APostion='relative' Text='Pencils' Atop='40' Aleft='0'></DTag>
              </div>
              <div className="bg-white rounded-md p-3">
                <p className={`${styles.section_ai_p} ${styles.title_ai_p}`}>Artificial Intelligence</p>
                <p className={`${styles.section_ai_p} ${styles.supject_ai_p}`}>
                  {page["ai-titel"][0].text}
                </p>
                <div
                  className={`grid justify-items-center ${styles.section_ai_p}`}
                >
                  <img src="/img/ai_logo.png"></img>
                </div>
                <p className={`${styles.section_ai_p} ${styles.desc_ai_p}`}>{page["ai-bio"][0].text}</p>
              </div>
            </div>
            <div className="col-span-1"></div>
          </div>
        </div>
      </div>
    </>
  );
}
