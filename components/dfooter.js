import styles from "@styles/Footer.module.css";

export default function Dfooter({ page }) {
  return (
    <>
      <div className={`${styles.container_footer_div} grid grid-rows-2  `}>
          <div className='grid justify-items-center content-end'>
              <p className={styles.p_footer}>{page["email-bio"][0].text}</p>
          </div>
          <div>
              <div className='grid justify-items-center'>
                  <div className={` ${styles.input_span_div} mt-1 rounded-md shadow-sm`}>

                      <input
                          type="email"
                          name="email-website"
                          id="email-website"
                          className={`${styles.input_footer}  h-10`}
                          placeholder="Enter email"
                      />
                      <span className={`${styles.input_footer_btn} inline-flex items-center px-3 h-10 border border-r-0  bg-gray-50`}>
                        Request Beta
                    </span>
                  </div>
              </div>
          </div>
      </div>
    </>
  );
}
