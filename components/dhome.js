import Image from "next/image";
import styles from "@styles/Home.module.css";
import DTag from "@components/dtag";

export default function DHome({ page }) {
  return (
    <>
      <div className="grid grid-cols-1">
        <div className={`grid grid-cols-12 ${styles.container_home_div}`}>

            <div className=' col-span-5 p-24'>

                <div className='grid grid-rows-4'>
                    <div>
                        <img
                        className={styles.logo_home_img}
                        src="/img/logo.png"
                        alt="logo"
                        ></img>
                    </div>
                    <div>
                        <img
                            className={styles.comming_home_img}
                            src="/img/comming.png"
                            alt="comming on"
                        ></img>
                    </div>
                    <div>
                        <div className={styles.bio_home_div}>{page.bio[0].text}</div>
                    </div>
                    <div className='grid grid-cols-4'>
                        <div className={`grid col-span-1 ${styles.vl}`}></div>
                        <div className={`grid col-span-3 ${styles.about_home_div}`}>{page.about_home[0].text}</div>
                    </div>

                </div>

            </div>

            <div className=' col-span-7 relative'>

                <img className='absolute top-20 bottom-4 right-36 z-10' src='/img/sample-1.png'/>
                <img className='absolute top-60 right-28 z-20' src='/img/sample-2.png'/>
                <img className='absolute bottom-20 left-0 z-10' src='/img/sample-3.png'/>
                <img className={`absolute top-0 left-0 w-full ${styles.bgk_home_img}`} src="/img/home_bgk.png"></img>
                <DTag APostion='absolute' Text='Person' Atop='44' ARight='96'></DTag>
                <DTag APostion='absolute' Text='Desk' ABottom='44' Aleft='36'></DTag>
                <DTag APostion='absolute' Text='Pencils' Atop='36' ARight='36'></DTag>
                <DTag APostion='absolute' Text='Water' ABottom='36' ARight='36'></DTag>
            </div>

        </div>

          {/* banner */}
          <div
              className={`${styles.banner_home_div} bottom-10 rounded-lg m-0 w-full grid grid-cols-3 content-center`}
          >
              <div className='grid justify-items-start'>
                  <div className={` ${styles.input_span_div} m-1`}>

                      <input
                          type="email"
                          name="email-website"
                          id="email-website"
                          className={`${styles.input_footer} h-10`}
                          placeholder="Enter email"
                      />
                      <span className={`${styles.input_footer_btn} inline-flex items-center px-3 h-10`}>
                          Request Beta
                      </span>
                  </div>
              </div>
              <div className="grid justify-items-center">
                  <div>
                      <Image
                          src="/img/logo_color.png"
                          alt="logo"
                          width={106}
                          height={45}
                          className={styles.logo_color_home_img}
                      ></Image>
                  </div>
              </div>
              <div className="grid justify-items-end mr-10 content-center">
                  <div className='flex space-x-3'>
                      <div className='flex-1'>
                          <img src='/img/logout.png'/>
                      </div>
                      <div className='flex-2'>
                          <p className={styles.banner_home_p_sign}>Sign in</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </>
  );
}
