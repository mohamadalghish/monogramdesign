import styles from "@styles/New.module.css";

export default function DNew({page}) {
    return (
        <>
            <div className={`${styles.container_new_div} grid lg:grid-cols-2 md:grid-cols-1`}>
                <div className='grid grid-rows-3'>
                    <div className={`${styles.title_new_div} grid justify-items-center content-center`}>News lorem at PicLab</div>
                    <div className='grid grid-cols-2'>
                        <div className={`grid justify-items-center ${styles.date_new_div}`}>JUL 18, 2021</div>
                        <div className={` ${styles.text_new_div}`}>PicLab generates $1 billion in revenue.</div>
                    </div>
                    <div className='grid grid-cols-2'>
                        <div className={`grid justify-items-center ${styles.date_new_div}`}>JUL 23, 2021</div>
                        <div className={` ${styles.text_new_div}`}>PicLab generates an additional $2 billion in revenue.</div>
                    </div>
                </div>
                {/*<div></div>*/}
            </div>
        </>
    )
}