import styles from "@styles/Home.module.css";


export default function DTag ({Text, Atop, Aleft, ARight, ABottom, APostion}) {

    return(
        <>
            <div className={`${APostion}  w-24 top-${Atop} left-${Aleft} right-${ARight} bottom-${ABottom} z-50 p-3`}>
                <p className={`${styles.tag_div}  text-white `} ># {Text}</p>
            </div>
        </>

    );
}