import styles from "@styles/Wh.module.css";

export default function Dwh({ page }) {
  return (
    <>
      <div className={styles.container_wh_div}>
        <div className="grid grid-rows-2 ">
          <div className="grid justify-items-center content-center">
            <p className={styles.text_wh_p}>{page["why-title"][0].text}</p>
          </div>
          <div>
            <div className={`${styles.card_wh_div} grid grid-cols-3 gap-4 m-5`}>

              <div className="bg-white h-96 rounded-lg grid grid-rows-4">
                <div className={`m-10 ${styles.card_tag_div}`}>All-in-one</div>
                <div className={`lg:m-10 md:m-1 sm:m-1 ${styles.card_title_div}`}>Lorem all of your file management on PicLab ipsum.</div>
                <div className={`lg:m-10 md:m-1 sm:m-1 ${styles.card_desc_div}`}>No more need to lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</div>
                <div>
                  <img className='w-full' src='/img/card_01.png' alt='card01'/>
                </div>
              </div>

              <div className="bg-white h-96 rounded-lg grid grid-rows-4">
                <div className={`m-10 ${styles.card_tag_div}`}>All-in-one</div>
                <div className={`lg:m-10 md:m-1 sm:m-1 ${styles.card_title_div}`}>Lorem all of your file management on PicLab ipsum.</div>
                <div className={`lg:m-10 md:m-1 sm:m-1 ${styles.card_desc_div}`}>No more need to lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</div>
                <div>
                  <img className='w-full' src='/img/card_01.png' alt='card01'/>
                </div>
              </div>

              <div className="bg-white h-96 rounded-lg grid grid-rows-4">
                <div className={`m-10 ${styles.card_tag_div}`}>All-in-one</div>
                <div className={`lg:m-10 md:m-1 sm:m-1 ${styles.card_title_div}`}>Lorem all of your file management on PicLab ipsum.</div>
                <div className={`lg:m-10 md:m-1 sm:m-1 ${styles.card_desc_div}`}>No more need to lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</div>
                <div>
                  <img className='w-full' src='/img/card_01.png' alt='card01'/>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </>
  );
}
