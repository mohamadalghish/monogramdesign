import Head from "next/head";
import Prismic from "@prismicio/client";
import DHome from "@components/dhome";
import Dai from "@components/dai";
import Dwh from "@components/dwh";
import Dfooter from "@components/dfooter";
import DNew from "@components/dnew";

export default function Home({ page }) {
  return (
    <>
      <Head>
        <title>PicLab</title>
        <meta name="description" content="Description for PicLab" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="">
        <DHome page={page} />
        {/* second sector */}
        <Dai page={page} />
        {/* third sector */}
        <Dwh page={page} />
        {/* fourth sector */}
        <DNew page={page} />
        {/* footer section */}
        <Dfooter page={page} />
      </main>
    </>
  );
}

export async function getStaticProps() {
  // Prismic API endpoint
  const apiEndpoint = process.env.PRISMIC_API_URL;
  // Access Token if the repository is not public
  const accessToken = process.env.PRISMIC_TOKEN;

  const client = Prismic.client(apiEndpoint, { accessToken });
  const response = await client.query(
    Prismic.Predicates.at("document.type", "monogramdsg"),
    {}
  );
  let dataRender = response.results[0].data;
  //console.dir(dataRender);

  return {
    props: {
      page: dataRender,
    },
  };
}
